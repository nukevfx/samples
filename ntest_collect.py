"""Test interactive components of the collect module.

These tests require the Nuke module. Furthermore, they can only be run in
interactive mode because we will test UI elements, that are not available when
running in terminal mode. In order to test, you can simply copy the whole
content of this file, launch Nuke in an interactive session, paste the code in
Nuke's script editor and execute it.

"""

import unittest

import nuke

from smartCommand import collect
from smartCommand.command import Command


class TestCollect(unittest.TestCase):
    """Test the functionality of the collect module."""

    def setUp(self) -> None:
        """Set up a test menu structure that we can then scan.

        Test
            A
            B
                B_sub1
                B_sub2
            C
                C_submenu
                    C_submenu_command
                C_sub1

        """
        self.menu_test = nuke.menu("Nuke").addMenu("Test")
        self.menu_test.addCommand("A")
        menu_b = self.menu_test.addMenu("B")
        menu_b.addCommand("B_sub1")
        menu_b.addCommand("B_sub2")

        menu_c = self.menu_test.addMenu("C")
        sub_menu_c = menu_c.addMenu("C_submenu")
        sub_menu_c.addCommand("C_submenu_command")
        menu_c.addCommand("C_sub1")

    def tearDown(self) -> None:
        """Remove 'Test' menu."""
        nuke.menu("Nuke").removeItem("Test")

    def test_load_commands(self) -> None:
        """Ensure that all commands get collected.

        This will collect all commands from our 'Test' menu and check that all
        commands are included in our expected result.
        """
        menu_list = collect.CommandFromMenuCollector().load_commands(
            self.menu_test
        )

        command_paths = [str(command) for command in menu_list]

        expected = [
            "Test/A",
            "Test/B/B_sub1",
            "Test/B/B_sub2",
            "Test/C/C_submenu/C_submenu_command",
            "Test/C/C_sub1",
        ]

        self.assertListEqual(expected, command_paths)

    def test_ignore_commands(self) -> None:
        """Ensure ignoring elements will remove them from our menu list.

        In this test we will create the following Command structure:

            menu_list: [
                "File/New Comp",
                "File/Open",
                "File/Close",
                "File/Recent Comp1",
                "File/Recent Comp2",
                "File/Recent Comp3"
            ]

            ignore_list: [
                "Open",
                "Recent Comp"
            ]

            Expected result: [
                "File/New Comp",
                "File/Close"
            ]
        """
        command1 = Command(None, "File/New Comp")
        command2 = Command(None, "File/Open")
        command3 = Command(None, "File/Close")
        command4 = Command(None, "File/Recent Comp1")
        command5 = Command(None, "File/Recent Comp2")
        command6 = Command(None, "File/Recent Comp3")

        menu_list = [
            command1,
            command2,
            command3,
            command4,
            command5,
            command6
        ]

        ignore_list = ["Open", "Recent Comp"]

        expected_menu_list = [command1, command3]
        collector = collect.CommandFromMenuCollector()
        filtered_menu_list = collector.ignore_commands(menu_list, ignore_list)

        self.assertListEqual(expected_menu_list, filtered_menu_list)


def main() -> None:
    """Run all tests."""
    unittest.main()


nuke.executeInMainThread(main)
