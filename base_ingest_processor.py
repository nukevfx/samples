"""Abstract base class for a processor to handle data ingestions."""

import abc
import logging
import time
from typing import Any, Dict
from threading import Lock

from smartElements.clogging import LEVEL
from smartElement.job import Job

# Module level lock to use shared resources among all processors.
LOCK = Lock()


class BaseIngestProcessor:
    """Abstract base class of a processor that runs when ingesting media."""

    __metaclass__ = abc.ABCMeta

    # The default settings to apply for a processor if not otherwise specified
    # in the smartElement's processor settings.
    DEFAULT_SETTINGS: Dict[str, Any] = {
        "enable": True,
    }

    def __init__(self, job: Job) -> None:
        """Initialize the ProcessorBase instance.

        Args:
            job: The job instance that holds information about the job to
                ingest.
        """
        self.job = job
        self.media = job.media
        self.lock = LOCK

        if job.settings and job.settings.get(self.name):
            self.settings = job.settings.get(self.name)
        else:
            self.settings = self.DEFAULT_SETTINGS

        module_path = f"smartElements.ingest_processors.{self.name}"
        self.logger = logging.getLogger(module_path)
        self.logger.setLevel(LEVEL)

    @property
    def name(self) -> str:
        """Get the name of the class.

        Returns:
            The name of the class.
        """
        return self.__class__.__name__

    def add_to_report(self, text: str) -> None:
        """Add the given string to the job report, add new line.

        Args:
            text: The message to add to the report.
        """
        self.job.add_to_job_report(text)

    def run(self) -> None:
        """Run the processor."""
        self._start = time.perf_counter()
        self.add_to_report("\n")
        self.add_to_report(self.name)
        self.add_to_report("-" * len(self.name))

        if not self.settings.get("enable"):
            self.add_to_report("Skip as not being enabled.")
            return

        self.process()
        duration = time.perf_counter() - self._start
        self.add_to_report(f"Finished in {duration}s")

    @abc.abstractmethod
    def process(self) -> None:
        """Perform any action that this processor is supposed to execute."""
        pass
