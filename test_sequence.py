"""Test the SequenceFinder under different inputs.

In here we test three test scenarios:

    1) The test path has a file sequence number and contains other frames.
    2) The test path has a file sequence number and contains no other frames.
    3) The test path has no file sequence number and this contains no other
       frames.

"""

import os
import shutil
import unittest

from convern import sequence
from convern.test import utils

# Constant values
# The regex pattern to use to split file basename, frame counter and file
# extension.
FILESEQUENCE_PATTERN = r"(.*\.)(\d+)(\.[a-z]+)"


class TestSequenceFinderSequence(unittest.TestCase):
    """Test SequenceFinder instance for a file sequence."""

    def setUp(self) -> None:
        """Set up test."""
        self.temp, self.paths = utils.create_test_files(1001, 1006)
        self.finder = sequence.SequenceFinder(
            self.paths[0], pattern=FILESEQUENCE_PATTERN
        )
        self.finder.scan()

    def tearDown(self) -> None:
        """Clean up after test."""
        shutil.rmtree(self.temp)

    def test_split_groups(self) -> None:
        """Ensure the file name base, padding and ext get split correctly."""
        filename, padding, ext = self.finder._split_groups()
        calculated = (filename, padding, ext)

        file_base = os.path.join(os.path.dirname(self.paths[0]), "test.")
        expected = file_base, 1001, ".exr"

        self.assertTupleEqual(expected, calculated)

    def test_scan(self) -> None:
        """Ensure we glob all five files of our file sequence."""
        self.assertListEqual(self.paths, self.finder._scan())

    def test_first_frame_is_1001(self) -> None:
        """The first frame of our file sequence should be 1001."""
        self.assertEqual(1001, self.finder.first)

    def test_last_frame_is_1005(self) -> None:
        """The last frame of our file sequence should be 1005."""
        self.assertEqual(1005, self.finder.last)

    def test_check_missing(self) -> None:
        """Removing frame 1003 should be in the missing list."""
        self.assertEqual(0, len(self.finder.missing))

        frame_1003 = self.paths[2]
        os.remove(frame_1003)

        self.finder.scan()
        self.assertEqual(1, len(self.finder.missing))
        self.assertEqual(frame_1003, self.finder.missing[0])


class TestSequenceFinderOneFrameSequence(unittest.TestCase):
    """Test SequenceFinder instance for a file sequence of 1 frame."""

    def setUp(self) -> None:
        """Set up test."""
        self.temp, self.paths = utils.create_test_files(1001, 1002)
        self.finder = sequence.SequenceFinder(
            self.paths[0], pattern=FILESEQUENCE_PATTERN
        )
        self.finder.scan()

    def tearDown(self) -> None:
        """Clean up after test"""
        shutil.rmtree(self.temp)

    def test_split_groups(self) -> None:
        """Ensure the file name base, padding and ext get split correctly."""
        filename, padding, ext = self.finder._split_groups()
        calculated = (filename, padding, ext)

        file_base = os.path.join(os.path.dirname(self.paths[0]), "test.")
        expected = file_base, 1001, ".exr"

        self.assertTupleEqual(expected, calculated)

    def test_scan(self) -> None:
        """Ensure we glob all five files of our file sequence."""
        self.assertListEqual(self.paths, self.finder._scan())

    def test_first_frame_is_1001(self) -> None:
        """The first frame of our file sequence should be 1001."""
        self.assertEqual(1001, self.finder.first)

    def test_last_frame_is_1005(self) -> None:
        """The last frame of our file sequence should be 1005."""
        self.assertEqual(1001, self.finder.last)


class TestSequenceFinderStaticFrame(unittest.TestCase):
    """Test SequenceFinder instance for a static frame."""

    def setUp(self) -> None:
        """Set up test."""
        self.temp, self.path = utils.create_static_test_file()
        self.finder = sequence.SequenceFinder(self.path,
                                              pattern=FILESEQUENCE_PATTERN)
        self.finder.scan()

    def test_no_split(self) -> None:
        """Raise ValueError when the file path has no file sequence number."""
        self.assertRaises(ValueError, self.finder._split_groups)

    def test_scan(self) -> None:
        """Ensure only the path member is in the files list."""
        self.assertEqual(1, len(self.finder.files))
        self.assertEqual(self.path, self.finder.files[0])

    def test_first_frame_is_1(self) -> None:
        """The first frame should be 1."""
        self.assertEqual(1, self.finder.first)

    def test_last_frame_is_1(self) -> None:
        """The first frame should be 1."""
        self.assertEqual(1, self.finder.last)
