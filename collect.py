"""Menu collector to collect all menu items recursively.

Examples:
    # Print all commands from the all menus.
    >>> from smartCommand import collect
    >>> collector = collect.CommandFromMenuCollector()
    >>> for command in collector.commands:
    ...     print(command.path)

    # Print all commands from xml.
    >>> from smartCommand import collect
    >>> xml = <enter absolute path of xml>
    >>> collector = collect.CommandFromXMLCollector(xml)
    >>> for command in collector.commands:
    ...     print(command.path)

    # Print all commands from directory.
    >>> from smartCommand import collect
    >>> directory = <enter absolute path of directory>
    >>> collector = collect.CommandFromDirectoryCollector(directory)
    >>> for command in collector.commands:
    ...     print(command.path)

"""

import abc
import os
from typing import List, Optional
from xml.etree import ElementTree

import nuke

from smartCommand import helper
from smartCommand.command import Command
from smartCommand.constants import IGNORE_CHARACTERS
from smartCommand.constants import SEP


class BaseCollector:
    """Base Collector class."""

    __metaclass__ = abc.ABCMeta

    def __init__(self) -> None:
        """Initialize the BaseCollector instance."""
        self.commands: List[str] = []
        self.collect_commands()

    @abc.abstractmethod
    def collect_commands(self) -> None:
        """Collect all commands for this Collector instance."""
        pass

    @staticmethod
    def ignore_commands(
            menu_list: List[str],
            ignore_list: Optional[List[str]] = None
    ) -> List[str]:
        """Ignore all elements from menu list that are in the ignore list.

        This will check the paths of the command and not include it if one of
        the sub strings from the given ignore list is included. Elements in
        ignore list can be sub strings of command's paths, see Example for
        further explanation.

        Args:
            menu_list: All Command instances to scan for.
            ignore_list: All strings to ignore from given command noted

        Example:
            # For the sake of demonstration we use strings in here. Command
            # instances are actually used.

            menu_list: [
                "File->New Comp",
                "File->Open",
                "File->Close",
                "File->Recent Comp1",
                "File->Recent Comp2",
                "File->Recent Comp3"
            ]

            ignore_list: [
                "Open",
                "Recent Comp"
            ]

            Result:
                [
                    "File->New Comp",
                    "File->Close",
                ]

            "File->Open" and all "File->Recent CompX" are removed because the
            sub strings "Open" and "Recent Comp" were part of these elements.

        Returns:
            Sequence of commands that are not listed in any ignore list.
        """
        ignore_list = ignore_list or []

        return [
            command
            for command in menu_list
            if not any(ignore in str(command) for ignore in ignore_list)
        ]


class CommandFromMenuCollector(BaseCollector):
    """Collect all menu items recursively."""

    def collect_commands(self) -> None:
        """Collect all commands in all menus."""
        for menu in nuke.menu("Nuke").items():
            menu_list = self.load_commands(menu)

            # Ensure to remove any elements from our ignore list.
            menu_list = self.ignore_commands(menu_list)

            self.commands.extend(menu_list)

    @staticmethod
    def load_commands(
            menu: nuke.Menu,
            separator: Optional[str] = SEP
    ) -> List[Command]:
        """Load all commands from given menu into list.

        This will iterate over all menus and submenus and load all children
        (commands and sub menus with sub command) as Command instances
        recursively from given menu into a list.

        Args:
            menu: Menu to load all children off of.
            separator: Separator to use for concatenating menu assembly.

        Returns:
            All children of given menu as Command instances.
        """
        menu_content = []
        path: List[str] = []

        def _recursive_load_commands(
            item: nuke.Menu,
            _separator: str,
            _path: Optional[List[str]] = None,
            ignore_characters: Optional[List[str]] = IGNORE_CHARACTERS
        ) -> None:
            """Add all commands as Command instances to a list.

            This will walk menus and sub menus recursively and add all commands
            as Command instances into a list. Menus themselves won't be added,
            just their containing commands.

            Args:
                item: menu to scan recursively fot MenuItems.
                _path: Current menu path. This gets stored for menu assembly.
                _separator: Separator to use for concatenating menu assembly.
                ignore_characters: Remove all these characters from the menu
                    path.

            Returns:
                All Commands of given menu read recursively inclusive all
                    commands in all nested menus.
            """
            _path = _path or []
            _path.append(item.name())

            for sub_item in item.items():
                if isinstance(sub_item, nuke.Menu):
                    _recursive_load_commands(sub_item, _separator, _path=_path)

                    # When we have reached the last element of a menu, we need
                    # to remove the menu itself so that it won't appear in
                    # upcoming commands that are under the menu we iterated
                    # over.
                    _path.pop()

                elif isinstance(sub_item, nuke.MenuItem):
                    # Separator are commands with an empty label that don't do
                    # anything. Skip these.
                    if not sub_item.name():
                        continue

                    _path.append(sub_item.name())

                    # Create and append Command item.
                    menu_path = _separator.join(_path)

                    # Remove all ignore characters.
                    menu_path = "".join(
                        [
                            char for char in menu_path
                            if char not in (ignore_characters or [])
                        ]
                    )

                    menu_content.append(
                        Command(
                            sub_item,
                            menu_path,
                            type_="ui_command",
                            hotkey=sub_item.shortcut(),
                        )
                    )

                    # Remove element name so that we can continue with the
                    # next element in our iteration.
                    _path.pop()

        _recursive_load_commands(menu, separator or SEP, _path=path)
        return menu_content


class CommandFromXMLCollector(BaseCollector):
    """Collect all menu items from a given menu xml."""

    def __init__(
            self,
            xml: str,
            ignore_characters: List[str] = IGNORE_CHARACTERS
    ) -> None:
        """Initialize the MenuFromXMLCollector instance.

        Args:
            xml: Absolute path of xml to parse for commands.
            ignore_characters: Remove all these characters from the menu path.
        """
        self.xml = xml
        self.ignore_characters = ignore_characters

        self.validate_xml()

        super().__init__()

    def validate_xml(self) -> None:
        """Check if xml exists and is well-formed.

        Write log when it is not well-formed.

        Raises:
            IOError: When xml does not exist.
        """
        if not os.path.isfile(self.xml):
            raise IOError(f"Cannot parse menu xml. No such file: {self.xml}")

        try:
            with open(self.xml, "r") as xml_file:
                ElementTree.fromstring(xml_file.read())
        except ElementTree.ParseError:
            helper.write_log(f"Skip non well formed menu collection: {self.xml}")

    def collect_commands(self) -> None:
        """Collect all commands in all menus."""
        tree = ElementTree.parse(self.xml)
        root = tree.getroot()

        collection_ = root.find("collection")
        if not collection_:
            return

        for element in collection_.findall("command"):
            if not element.text:
                continue

            menu_path = "".join(
                [
                    char for char in element.text
                    if char and char not in self.ignore_characters
                ]
            )

            # Create command instance and append to commands.
            command = Command(
                None,
                menu_path,
                color=element.get("color"),
                file_=self.xml,
                hotkey=element.get("hotkey"),
                type_="xml",
            )

            self.commands.append(command)


class CommandFromDirectoryCollector(BaseCollector):
    """Collect all menu items from a given folder."""

    def __init__(
            self,
            path: str,
            module_ext: str = ".py",
            ignore_prefix: str = "_"
    ) -> None:
        """Initialize the CommandFromDirectory instance.

        Args:
            path: Absolute path of root to scan for commands.
            module_ext: Scan for all files with this extension.
            ignore_prefix: Skip all commands that start with this character.
        """
        self.path = path
        self.module_ext = module_ext
        self.ignore_prefix = ignore_prefix

        self.ensure_path()

        super().__init__()

    def ensure_path(self) -> None:
        """Create path location if it doesn't exist, yet.

        Raises:
            OSError: When we cannot create the folder at the given path.
        """
        if os.path.isdir(self.path):
            return

        try:
            os.makedirs(self.path)
        except OSError:
            raise OSError(f"Cannot create scriptlets root at: {self.path}")

    def collect_commands(self) -> None:
        """Collect all commands recursively from root."""
        for root, _, files in os.walk(self.path, topdown=True):
            for name in files:
                if not name.endswith(self.module_ext):
                    continue
                if name.startswith(self.ignore_prefix):
                    continue

                full_path = os.path.join(root, name)
                docstring = helper.get_module_docstring(full_path)

                command_path = os.path.join(root, os.path.splitext(name)[0])
                command_path = command_path.replace(self.path, "")
                if command_path.startswith(os.sep):
                    command_path = "".join(command_path[1:])

                # Create command instance and append to commands.
                command = Command(
                    None,
                    command_path,
                    file_=full_path,
                    type_="scriptlet",
                    tooltip=docstring,
                )
                self.commands.append(command)
