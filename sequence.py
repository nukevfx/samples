"""Detect all files of a sequence from one given image in a light weight way.

This module contains a SequenceFinder that will automatically detect the
full sequence of a given image path. This will use a regex to split off the
frame padding of a given path and glob for files with the same name but a
different file sequence number in order to get the full file sequence. It will
also provide information about the first- and last frame as well as frames that
are missing in the sequence. This works basically like a light version of
fileseq or pyseq but with less overhead.

Usage:
    >>> from convern.sequence import SequenceFinder
    >>> finder = SequenceFinder("path/to/imagefile.1001.exr")
    >>> finder.scan()

    # Let's print all image sequence files for the given path.
    >>> finder.files
    [
        'path/to/image.1001.exr',
        'path/to/image.1002.exr',
        'path/to/image.1003.exr',
        ...
        'path/to/image.1010.exr',
    ]

    # Let's print the first sequence number of our image sequence.
    >>> finder.first
    1001

    # Let's print the last sequence number of our image sequence.
    >>> finder.last
    1010

    # The sequence finder detects frames in the sequence that are missing.
    # Let's remove the middle frame
    >>> import os
    >>> os.remove(finder.files[5])
    >>> finder = SequenceFinder("path/to/imagefile.1001.exr")
    >>> finder.scan()
    >>> finder.missing
    ["path/to/imagefile.1006.exr"]

"""

import glob
import os
import re
from typing import List, Tuple

from convern.configuration import Configuration


# We want to keep the member calculation private as the user doesn't need to
# handle these calculation steps themselves.
# pylint: disable=too-few-public-methods
class SequenceFinder:
    """Find the sequence of a given file path in the current working dir."""

    def __init__(self, path: str, pattern: str = ""):
        """Initialize the SequenceFinder instance.

        Args:
            path: Absolute path of file to search for file sequence.
            pattern: Pattern to use for splitting the file name base, file
                sequence number and file extension from the given path.
        """
        self.path = path
        self.pattern = pattern or Configuration()["filesequence_pattern"]

        self.files: List[str] = []
        self.first = 1
        self.last = 1
        self.missing: List[str] = []

    def scan(self) -> None:
        """Perform scan process and search for all images of the sequences."""
        self.files = self._scan()
        self.first = self._get_frame(self.files[0])
        self.last = self._get_frame(self.files[-1])
        self.missing = self._get_missing()

    def _split_groups(self) -> Tuple[str, int, str]:
        """Split groups for file path to file name base, padding and ext.

        These values are processed further in order to detect the full file
        sequence of the path member.

        Returns:
            The file name base, the file sequence number and the file
                extension from the path member.

        Raises:
            ValueError: When the file sequence pattern doesn't match due to
                the fact that the path member does not contain a file sequence
                number.
        """
        match = re.search(self.pattern, self.path)
        if not match:
            raise ValueError(
                "File path does not contain a file sequence number."
            )

        return match.group(1), int(match.group(2)), match.group(3)

    def _scan(self) -> List[str]:
        """Glob for files with the file path structure of the path member.

        This will use the split file name, file sequence number and file
        extension to create a glob pattern where the file sequence number is
        variable.

        Returns:
             Absolute file paths of matching file sequences, sorted
                alphabetically.
        """
        try:
            filename_base, _, extension = self._split_groups()
        except ValueError:
            return [self.path]

        pattern = f"{filename_base}*{extension}"

        return sorted(file_ for file_ in glob.glob(pattern))

    def _get_frame(self, path: str) -> int:
        """Extract the file sequence number of the given path.

        Args:
            path: Path to extract file sequence number of.

        Returns:
            The extracted file sequence number if the given path contains one,
                otherwise 1.
        """
        match = re.search(self.pattern, path)
        if not match:
            return 1
        return int(match.group(2))

    def _get_missing(self) -> List[str]:
        """Get a list of all missing frames in the sequence.

        Returns:
            Sequence of absolute paths of frames that are missing between the
                first and last frame of the sequence.
        """
        try:
            filename_base, _, extension = self._split_groups()
        except ValueError:
            return []

        def assemble_path(filesequence_number: int) -> str:
            """Assemble file path using given filesequence number.

            Args:
                filesequence_number: File sequence number to use for path
                    assembly.

            Returns:
                The assembled file path that uses the given filesequence
                    number.
            """
            return f"{filename_base}{filesequence_number}{extension}"

        files = (assemble_path(frame) for frame in
                 range(self.first, self.last + 1))
        return [file_ for file_ in files if not os.path.isfile(file_)]
