"""Idle detection to automatically stop and start a time logger via a thread.

We register a custom event on the keyPress event of Nuke's main window.
Whenever the user presses a key, we will update the timestamp of the latest
change. Thus we will be able to detect when the last change was happening.

"""

import logging
import os
import time

from PySide2 import QtCore

import smartLog
from smartLog import settings
from smartLog.constants import LATEST_CHANGE


class ChangeObserver(QtCore.QObject):
    """Register the last change in the main window."""

    def __init__(self) -> None:
        """Initialize the ChangeObserver and ensure environment variable."""
        super().__init__()

        os.environ[LATEST_CHANGE] = str(int(time.time()))

    def eventFilter(
            self,
            obj: QtCore.QObject,
            event: QtCore.QEvent
    ) -> bool:
        """Override eventFilter to inject code for various event types.

        Args:
            obj: The object to drive.
            event: The event that got fired.

        Returns:
            False when we handled a MouseMove event so that we could inject
                our custom code, eventFilter function otherwise so that we can
                further pass the event to other receivers.
        """
        if event.type() == QtCore.QEvent.MouseMove:
            os.environ[LATEST_CHANGE] = str(int(time.time()))

            # Explicitly returning False here so that we have just injected
            # our idle detection but leave this event to other event
            # installments. The code above will still be executed which is what
            # we want here.
            return False

        return QtCore.QObject.eventFilter(self, obj, event)


class IdleObserver:
    """Check in a constant interval if we got idle."""

    # The interval in milliseconds to check via a timer if we got idle.
    INTERVAL = 1000

    # We get idle after that amount of seconds when the mouse cursor has not
    # moved inside our current DCC session.
    IDLE_AFTER_SECONDS = settings.Settings().get("idle_interval")

    def __init__(self) -> None:
        """Initialize the IdleObserver instance."""
        self.logger = logging.getLogger(__name__)

        self._timer = QtCore.QTimer()
        self._timer.setInterval(self.INTERVAL)
        self._timer.timeout.connect(self._check)

        self._started = False
        self._stopped = False

    def _check(self) -> None:
        """Check if we got idle and if so stop the timer or resume it."""
        current_timestamp = int(time.time())
        latest_change = int(os.environ[LATEST_CHANGE])

        delta = current_timestamp - latest_change

        if delta > self.IDLE_AFTER_SECONDS:
            if self._stopped:
                return

            smartLog.SMARTLOG_TIME_LOGGER.stop()
            self._started = False
            self._stopped = True

        else:
            if self._started:
                return

            smartLog.SMARTLOG_TIME_LOGGER.start()
            self._started = True
            self._stopped = False

    def start(self) -> None:
        """Start the idle observer."""
        self._timer.start()
