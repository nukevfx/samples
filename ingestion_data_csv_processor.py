"""Add custom pre- and post processors.

This module serves to initialize and register custom processors that run on
each ingestion job -before- the main ingestion processors and/or -after- the
main ingestion processors. Each ingestion gets done via a smartElement's job
instance. A job instance runs multiple processors. The main ingestion
processors are run in the following order:

1) smartElements.processors.copier.Copier:
   Copy all media files to destination.

2) smartElements.processors.preview_creator.PreviewCreator
   Create a preview for a media.

3) smartElements.processors.data_extractor.DataExtractor
   Extract information about the media.

You can basically do anything you need before and/ or after the main
processors run. Each processor holds a job argument that contains detailed
information about the media to ingest.

A job contains the following attributes, methods and properties that you can
refer to if needed:

# Attributes:
- media -> smartElements.media.Media: Media that gets ingested. Contains
      information about the frames to ingest including all frames of a sequence
      if the media is a sequence.
- settings -> dict: All settings of all smartElements processors, including
      the main processors and custom processors.

# Methods:
- add_to_job_report(str) -> None: Add given string to the report.

# Properties:
- parent_dst -> str: The parent directory of the destination to copy to.
- meta_dir -> str: Absolute path to meta directory for the media to ingest.
- report_path -> str: Absolute path to report.

Each processor derives from the abstract `BaseProcessor` that can be found in:
`smartElements.processor.base_processor`. Each processor must implement a
`process` method. In here you can execute whatever needs to be executed for the
specific processor.

Please have a look at the above mentioned processors to see how the processors
work. In this module we illustrate an example processor that we add to
smartElements by defining its logic (in this case adding ingestion data to
a csv file) and adding it to the custom post processors by calling
`osl.register_post_processor(IngestionDataCSVProcessor)`.

Each processor can reference (but does not necessarily need to) some settings
so that you can make it configurable. These settings can be stored in the
smartElement's settings. They can be found in the smartElement's settings under
the 'Advanced' tab.

Each ingestion job writes a job log file. Inside each processor you can call
the processor's `add_to_report` method to write anything to the job log file.
In addition, each processor has a logger instance (that is defined in the
abstract base_processor) that you can use to your advantage to log certain
information when needed.

For more information about custom pre and post processors please also have a
look at smartElement's written documentation:
https://www.cragl.com/smartElementsTut and https://www.cragl.com/smartElements

"""

import csv
import datetime
import json
import os
import time

from smartElements import media
from smartElements import osl
from smartElements.constants import META
from smartElements.ingest_processors.base_ingest_processor import BaseIngestProcessor


# Need to ignore due to missing dependencies for this code sample.
class IngestionDataCSVProcessor(BaseIngestProcessor):  # type: ignore[misc]
    """Example processor to illustrate the workflow of a custom processor.

    This processor is an example to illustrate how to implement a custom
    processor. This processor will keep track of the ingested media by writing
    information about the ingested media to a csv file.

    We run this processor -after- the main processors have finished. We will
    extract data of the ingested media's meta.json and add this information
    to a csv file that contains data about all media ingestions.

    """

    # The csv fields to write to the csv file.
    FIELDS = [
        "date",
        "name",
        "format",
        "frames",
        "size",
        "source",
        "stack",
        "list"
    ]

    @property
    def csv_path(self) -> str:
        """Get the absolute path to the csv file.

        Returns:
            The absolute path to the csv file.
        """
        return os.path.expandvars(self.settings["path"]) or ""

    @staticmethod
    def datetime_human_readable() -> str:
        """Return human-readable date and time from given timestamp.

        Returns:
            Human-readable date and time from current timestamp in the
                format: month/day/year hour:minute:second
        """
        date_object = datetime.datetime.fromtimestamp(time.time())
        return time.strftime("%m/%d/%y %H:%M:%S", date_object.timetuple())

    def ensure_csv(self) -> None:
        """Ensure the csv file from configured path exists.

        If the csv file does not exist, we will create it with the needed
        headers.
        """
        csv_path = self.csv_path
        if os.path.isfile(csv_path):
            return

        csv_root = os.path.dirname(csv_path)
        if not os.path.isdir(csv_root):
            os.makedirs(csv_root)

        with open(csv_path, "w") as file_:
            csv_writer = csv.DictWriter(file_, fieldnames=self.FIELDS)
            csv_writer.writeheader()

    def process(self) -> None:
        """Add information about the ingested media to a csv file."""
        self.ensure_csv()

        meta_path = os.path.join(self.job.parent_dst, META, "meta.json")
        if not os.path.isfile(meta_path):
            self.logger.info("Media meta file does not exist. Exiting.")
            return

        with open(meta_path, "r") as file_:
            meta = json.load(file_)

        ingest_to_list_path = os.path.dirname(self.job.parent_dst)
        stack_name = os.path.basename(os.path.dirname(ingest_to_list_path))

        self.logger.info("Writing ingestion data to: %s", self.csv_path)
        with open(self.csv_path, "a") as file_:
            csv_writer = csv.DictWriter(file_, fieldnames=self.FIELDS)
            width = meta.get("width", "")
            height = meta.get("height", "")
            csv_writer.writerow(
                {
                    "date": self.datetime_human_readable(),
                    "name": self.job.media.base,
                    "format": f"{width}x{height}",
                    "frames": meta.get("frames"),
                    "size": media.human_readable_size(meta["size"]),
                    "source": meta["source"],
                    "stack": stack_name,
                    "list": ingest_to_list_path,
                }
            )


# Register processors. We can define pre-processors that run -before- the main
# processors and post-processors that run -after- the main processors:
# osl.register_pre_processor(<processor class>)
# osl.register_post_processor(<processor class>)

# As an example, we register our IngestionDataCSVProcessor to run -after- the
# main processors have run. Uncomment the following line so that the custom
# processor gets registered to smartElements.
osl.register_post_processor(IngestionDataCSVProcessor)
