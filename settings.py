"""Settings store being loaded and saved to a settings.json file."""

import json
import logging
import os
from typing import Any, Dict

from smartElements import paths
from smartElements.constants import JSON_INDENT
from smartElements.constants import LOGGING_LEVELS
from smartElements.templates import DEFAULT_SETTINGS


class Settings:
    """Settings being loaded and saved to settings.json file."""

    SETTINGS_PATH = os.path.join(paths.get_package_dir(), "settings.json")
    LOGGER = logging.getLogger(__name__)

    def __init__(self) -> None:
        """Initialize the Settings instance."""
        if not os.path.isfile(self.SETTINGS_PATH):
            self.check_settings()

        self._settings = self.load_settings()

        self.LOGGER.setLevel(LOGGING_LEVELS[self._settings["logging_level"]])

    @classmethod
    def check_settings(cls) -> None:
        """Ensure settings file exists and all key-values are contained.

        We will add any key-value pair from the DEFAULT_SETTINGS that are not
        contained inside the physical settings.json on disk. This ensures we
        can add additional key-value pairs in the future and ensure they exist
        for the user.

        """
        paths.ensure_directory(paths.get_package_dir())

        if not os.path.isfile(cls.SETTINGS_PATH):
            with open(cls.SETTINGS_PATH, "w") as file_:
                cls.LOGGER.debug("Creating setting file in %s",
                                 cls.SETTINGS_PATH)

                json.dump(DEFAULT_SETTINGS, file_, indent=JSON_INDENT)
                return None

        settings = Settings().load_settings()
        for key, value in DEFAULT_SETTINGS.items():
            if key not in settings:
                cls.LOGGER.debug("Adding to settings.json: %s=%s", key, value)
                settings[key] = value

        # Ensure we have all default ingest and import processor settings in
        # here as well. The import_processors and ingest_processors setting are
        # a dict of nested dicts. When these dicts existed then the nested
        # dicts would not be updated, however we rely on having default
        # settings for our import and ingest processors.
        for name in ("import_processors", "ingestion_setup"):
            for key, value in DEFAULT_SETTINGS[name].items():
                if key not in settings[name]:
                    cls.LOGGER.debug(
                        "Adding %s settings to settings.json: %s=%s", name,
                        key, value
                    )
                    settings[name][key] = value

        with open(cls.SETTINGS_PATH, "w") as file_:
            json.dump(settings, file_, indent=JSON_INDENT)

    @classmethod
    def load_settings(cls) -> Dict[str, Any]:
        """Load settings file without the need of a model.

        Returns:
            The parsed settings file.
        """
        with open(cls.SETTINGS_PATH, "r") as file_:
            return json.load(file_) or {}

    def save_setting(self, key: str, value: Any) -> Dict[str, Any]:
        """Save the given value to the given key and return settings.

        Args:
            key: The name of the key to update.
            value: The new value to set for the given key.

        Returns:
            The updated settings.
        """
        with open(self.SETTINGS_PATH, "r") as file_:
            settings: Dict[str, Any] = json.load(file_)

        settings[key] = value

        with open(self.SETTINGS_PATH, "w") as file_:
            json.dump(settings, file_, indent=JSON_INDENT)

        return settings

    def get(self, key: str) -> Any:
        """Get settings value for given key.

        Args:
            key: Name of key to get settings value for.

        Returns:
            The value of the asked settings key.
        """
        return self._settings[key]

    def reload(self) -> None:
        """Reload the settings."""
        self._settings = self.load_settings()
